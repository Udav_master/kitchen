

/*=============================================================
    Authour URI: www.binarytheme.com
    License: Commons Attribution 3.0

    http://creativecommons.org/licenses/by/3.0/

    100% To use For Personal And Commercial Use.
    IN EXCHANGE JUST GIVE US CREDITS AND TELL YOUR FRIENDS ABOUT US

    ========================================================  */

(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {
            /*====================================
             CUSTOM LINKS SCROLLING FUNCTION
            ======================================*/

            $('header a[href*=#]').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
               && location.hostname == this.hostname) {
                    var $target = $(this.hash);
                    $target = $target.length && $target
                    || $('[name=' + this.hash.slice(1) + ']');
                    if ($target.length) {
                        var targetOffset = $target.offset().top;
                        $('html,body')
                        .animate({ scrollTop: targetOffset }, 800); //set scroll speed here
                        return false;
                    }
                }
            });


            /*====================================
               WRITE YOUR SCRIPTS BELOW
           ======================================*/

            new WOW().init();

            $('.album').masonry({
                columnWidth: '.kitchen-box',
                itemSelector: '.kitchen-box',
                transitionDuration: '0.5s'
            });

            $('.gallery-filters a').on('click', function(e){
                e.preventDefault();
                if(!$(this).hasClass('active')) {
                    $('.gallery-filters a').removeClass('active');
                    var clicked_filter = $(this).attr('class').replace('filter-', '');
                    $(this).addClass('active');
                    if(clicked_filter != 'all') {
                        $('.kitchen-box:not(.' + clicked_filter + ')').css('display', 'none');
                        $('.kitchen-box:not(.' + clicked_filter + ')').removeClass('kitchen-box');
                        $('.' + clicked_filter).addClass('kitchen-box');
                        $('.' + clicked_filter).css('display', 'block');
                        $('.album').masonry();
                    }
                    else {
                        $('.album > div').addClass('kitchen-box');
                        $('.album > div').css('display', 'block');
                        $('.album').masonry();
                    }
                }
            });

            $(window).on('resize', function(){ $('.album').masonry(); });

            $('.kitchen').magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: 'Изображение не может быть загружено.',
                    titleSrc: function(item) {
                        return item.el.find('.kitchen-description').find('p').text();
                    }
                },
                callbacks: {
                    elementParse: function(item) {
                        item.src = item.el.find('img').attr('src');
                    }
                }
            });

            $('.order-link').magnificPopup({
                midClick: true
            });

            $('.testimonial-active').html('<p>' + $('.testimonial-single:first p').html() + '</p>');
            $('.testimonial-single:first .testimonial-single-image img').css('opacity', '1');

            $('.testimonial-single-image img').on('click', function() {
                $('.testimonial-single-image img').css('opacity', '0.5');
                $(this).css('opacity', '1');
                var new_testimonial_text = $(this).parent('.testimonial-single-image').siblings('p').html();
                $('.testimonial-active p').fadeOut(300, function() {
                    $(this).html(new_testimonial_text);
                    $(this).fadeIn(400);
                });
            });

        },

        initialization: function () {
            mainApp.main_fun();

        }

    };
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));
