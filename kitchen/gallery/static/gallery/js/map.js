function initMap() {
  var myLatLng = {lat: 53.9329123, lng: 27.4546865};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map
  });
}
