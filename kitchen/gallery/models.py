from django.db import models


class Kitchen(models.Model):
    STYLE_CHOICES = (
        ("classic", "Классика"),
        ("modern", "Модерн"),
        ("provence", "Прованс"),
        ("country", "Кантри"),
        ("england", "Английский"),
        ("italian", "Итальянский"),
        ("neoclassic", "Неоклассика"),
    )
    MATERIAL_CHOICES = (
        ("block", "Массив"),
        ("enamel", "Эмаль"),
        ("veneer", "Шпон"),
        ("mdf", "МДФ"),
        ("plastic", "Пластик"),
        ("membrane", "Пленка"),
    )
    name = models.CharField(max_length=20)
    description = models.TextField(max_length=130, blank=True)
    style = models.CharField(max_length=20, choices=STYLE_CHOICES)
    material = models.CharField(max_length=20, choices=MATERIAL_CHOICES)
    price = models.IntegerField()
    image = models.ImageField(blank=True)

    def __str__(self):
        return self.name
