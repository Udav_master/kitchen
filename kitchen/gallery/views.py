from django.views.generic import FormView

from .models import Kitchen
from .forms import ContactForm


class GalleryView(FormView):
    template_name = "gallery/index.html"
    form_class = ContactForm
    success_url = "#"

    def get_context_data(self, **kwargs):
        context = super(GalleryView, self).get_context_data(**kwargs)
        context["kitchens"] = Kitchen.objects.all()
        return context

    def form_valid(self, form):
        form.send_mail()
        return super(GalleryView, self).form_valid(form)
