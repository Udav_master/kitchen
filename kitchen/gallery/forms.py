from django import forms
from django.core.mail import send_mail

from django.conf import settings


class ContactForm(forms.Form):
    name = forms.CharField(max_length=20, label="", widget=forms.TextInput(attrs={"placeholder": "Ваше имя"}))
    phone = forms.CharField(max_length=20, label="", widget=forms.TextInput(attrs={"placeholder": "Номер телефона"}))
    message = forms.CharField(max_length=300, label="", required=False,
                              widget=forms.Textarea(attrs={"placeholder": "Сообщение"}))

    def create_message(self):
        message = ""
        for field in self.cleaned_data:
            message += field + ": " + self.cleaned_data[field] + "\n"
        return message

    def send_mail(self):
        message = self.create_message()
        send_mail("Заказ", message, settings.EMAIL_HOST_USER, ["jouny2013@gmail.com"])
